TEMPLATE_TYPE=${1}
#clean
rm -rf cicd Makefile && \
#download script cicd
git archive --remote=ssh://git@bitbucket.org/newrahmat/template.git --format=tar --output="cicd.tar" cicd && tar -xvf cicd.tar && rm -rf cicd.tar && \
#download template
mkdir -p cicd/template && \
git archive --remote=ssh://git@bitbucket.org/newrahmat/template.git --format=tar --output="${TEMPLATE_TYPE}.tar" ${TEMPLATE_TYPE} && tar -xvf ${TEMPLATE_TYPE}.tar -C cicd/template/ && rm -rf ${TEMPLATE_TYPE}.tar

